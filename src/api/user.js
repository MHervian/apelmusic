import Axios from "axios";

export const APIRequest = Axios.create({
  // baseURL: "http://localhost:8471/api",
  baseURL: "http://52.237.194.35:2032/api",
});

const validateToken = (token) => {
  return APIRequest.post("/User/tokenidentity", { token: token });
};

export { validateToken };
